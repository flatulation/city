use ::reqwest::blocking::Client;
use clap::{App, AppSettings, Arg, SubCommand};
use reqwest::blocking as reqwest;
use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
struct CityFile {
    api_key: String,
}

fn key(site_name: &str, passwd: &str) -> [u8; 32] {
    reqwest::get("https://neocities.org/api/key").
}

fn main() {
    let clone = SubCommand::with_name("clone")
        .arg(Arg::with_name("site_name").required(true).takes_value(true));

    let matches = App::new("city")
        .setting(AppSettings::SubcommandRequired)
        .subcommand(clone)
        .get_matches();

    match matches.subcommand() {
        ("clone", Some(sub)) => {
            println!("clone {}", sub.value_of("site_name").unwrap());
        }
        _ => unreachable!(),
    };
}
